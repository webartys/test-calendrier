<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use Validator;



class EventController extends Controller
{
    public function event_add_post(Request $request) {
        // dd($request);
        $event = new Event();
        $validator = Validator::make($request->all(), [
            'hour_start' => 'required|date_format:H:i',
            'title' => 'required',
        ]);
        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
            }
        $event->title = $request->title;
        $event->date_start = $request->date_modal . " " . $request->hour_start;
        $event->save();

        return redirect()->back()->with(['message' => "Rendez-vous ajouté"]);


    }

    public function event_edit_post(Request $request) {
        // dd($request);
        $event = Event::find($request->id);
        $validator = Validator::make($request->all(), [
            'hour_start' => 'required|date_format:H:i',
            'title' => 'required',
        ]);
        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
            }
        $event->title = $request->title;
        $event->date_start = date("Y-m-d", strtotime($event->date_start)) . " " . $request->hour_start;
        $event->save();

        return redirect()->back()->with(['message' => "Rendez-vous modifié"]);

    }

    public function event_delete_post(Request $request) {
        $event = Event::find($request->id);
        $event->delete();
        return redirect()->back()->with(['message' => "Rendez-vous supprimé"]);
    }
}
