<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;

class FrontController extends Controller
{
    public function homepage() {
        $events = Event::all();
        return view('front.index', [
            'events' => $events
        ]);
    }
}
