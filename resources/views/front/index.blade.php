@extends('layouts.front')
@section('front.content')
    <div class="container">
        <h1 class="text-center pt-5">Calendrier</h1>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(session()->has('message'))
            <div class="alert alert-success text-center">
                <span class="float-right" > <a href="" class="text-muted"><i class="fas fa-times"></i></a></span>
                {{ session()->get('message') }}
            </div>
        @endif
        <div id='loading'>loading...</div>
        <div id='calendar'></div>
    </div>

@endsection
