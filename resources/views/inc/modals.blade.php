{{-- -------------------Modal ADD------------- --}}

<div class="modal" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="" action="{{ route('event.add.post') }}" method="post">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Rendez-vous</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" name="title" value="" placeholder="Titre" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="hour_start">Heure : </label>
                        <input type="time" name="hour_start" value="" required>
                        {{-- <label for="hour_end">Fin</label>
                        <input type="time" name="hour_end" value=""> --}}
                        <input type="hidden" name="date_modal" value="" id="date_modal">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- -------------------Modal UPDATE DELETE------------- --}}
<div class="modal" tabindex="-1" role="dialog" id="delModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="" action="{{ route('event.edit.post') }}" method="post">
                @csrf
                <input type="hidden" name="id" value="" id="idEvent_edit">

                <div class="modal-header">
                    <h5 class="modal-title">Rendez-vous</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        Modifier le rendez-vous ?
                    </div>
                    <div class="form-group">
                        <input type="text" name="title" value="" placeholder="Titre" class="form-control"  id="title"  required>
                    </div>
                    <div class="form-group">
                        <label for="hour_start">Nouvelle heure : </label>
                        <input type="time" name="hour_start" value="" id="hour_start" required>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-info">Modifier</button>
            </form>
                    <form class="" action="{{ route('event.delete.post') }}" method="post">
                        @csrf
                        <input type="hidden" name="id" value="" id="idEvent">
                        <button type="submit" class="btn btn-danger">Supprimer</button>
                    </form>
                </div>

        </div>
    </div>
</div>
