<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>calendar</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link href="{{ asset('libs/fullcalendar/lib/main.css') }}" rel='stylesheet' />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>

        <script src={{ asset('libs/fullcalendar/lib/main.js') }}></script>
        <script src={{ asset('libs/fullcalendar/lib/locales-all.js') }}></script>
        {{-- <script type="module" >
            import { Calendar } from '@fullcalendar/core';
            import googleCalendarPlugin from '@fullcalendar/google-calendar';
        </script> --}}
        <script>



            document.addEventListener('DOMContentLoaded', function() {
                var events = @json($events);

                var calendarEl = document.getElementById('calendar');

                var calendar = new FullCalendar.Calendar(calendarEl, {
                    headerToolbar: {
                      left: 'prev,next today',
                      center: 'title',
                      right: 'dayGridMonth,listYear'
                    },

                    displayEventTime: false, // don't show the time column in list view
                    googleCalendarApiKey: 'AIzaSyDcnW6WejpTOCffshGDDb4neIrXVUA1EAE',

                    events: 'en.usa#holiday@group.v.calendar.google.com',

                    initialView: 'dayGridMonth',
                    locale: 'fr',

                    dateClick: function(info) {
                        var inputDate = document.getElementById('date_modal');
                        inputDate.value = info.dateStr;
                         $('#myModal').modal('show');
                          // change the day's background color just for fun
                          // info.dayEl.style.backgroundColor = 'yellow';
                    },
                    eventClick: function(info) {
                        console.log(info.event)
                        var idEvent = document.getElementById('idEvent');
                        var idEvent_edit = document.getElementById('idEvent_edit');
                        var hourEvent = document.getElementById('hour_start');
                        var dateEvent = document.getElementById('date_modal');
                        var titleEvent = document.getElementById('title');
                        idEvent.value = info.event.id;
                        idEvent_edit.value = info.event.id;
                        titleEvent.value = info.event.title;
                        hourEvent.value = info.event.start;
                        dateEvent.value = info.dateStr;
                        $('#delModal').modal('show');
                        // opens events in a popup window
                        window.open(info.event.url, 'google-calendar-event', 'width=700,height=600');

                        info.jsEvent.preventDefault() // don't navigate in main tab
                    },
                    loading: function(bool) {
                      document.getElementById('loading').style.display =
                        bool ? 'block' : 'none';
                    }
                });
                    calendar.batchRendering(function() {
                    calendar.changeView('dayGridMonth');
                    if(events.length > 0) {
                        for (var i = 0; i < events.length; i++) {
                            console.log(events[i].id)
                            calendar.addEvent({ title: events[i].title, start: events[i].date_start, id: events[i].id });
                        }
                    }
                });
                calendar.render();
            });

        </script>
    </head>
    <body>

        @yield('front.content')

        @include('inc/modals')

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    </body>
</html>
