<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\EventController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontController::class, 'homepage'])->name('homepage');
Route::post('event-add-post', [EventController::class, 'event_add_post'])->name('event.add.post');
Route::post('event-edit-post', [EventController::class, 'event_edit_post'])->name('event.edit.post');
Route::post('event-delete-post', [EventController::class, 'event_delete_post'])->name('event.delete.post');
